Teste para avaliação Frontend Junior React
==========================================

Este é o arquivo de requisitos para o teste de candidatos à vaga de desenvolvedor Backend Python Junior.

Seu objetivo será programar uma API em Flask que lida com um CRUD de usuários.

Crie um repositório **público** no GitHub ou GitLab.
Depois de finalizado, envie o link do repositório para o avaliador que entrou em contato.

É recomendado usar a classe **MethodView presente no Flask** para a programação de cada método HTTP, porém será
permitido a programação usual do Flask com o uso do decorador @app.route(endpoint, methods=[...]).

------

Modelo de dados
---------------

Crie dentro da pasta models a classe que irá representar os dados do usuário.

**User**

A classe User irá conter os dados e métodos relacionados ao modelo do usuário.

Os atributos (públicos) presentes no usuário são:

* _id : string - Chave primária que será utilizada no mongo. Inicie um novo usuário com um hash aleatório (garanta tamanho suficiente para não haver conflitos)
* first_name : string - Primeiro nome
* last_name : string - Sobrenome
* login : string - Login do usuário
* password : string - hash do password do usuário (será detalhado abaixo)

Métodos da classe User:

* \[static\] hash_password(plaintext_password : string) -> None
    * Este método irá criar um hash do password do usuário, para não salvar o plaintext no banco. Utilizar o método bcrypt da biblioteca passlib.hash
* validate_password(plaintext_password : string) -> bool
    * Este método verifica se o password passado em plaintext é o gerador do hash que está presente no atributo self.password.
    * Retorna True se o password foi validado, ou False se não é o password gerador.


API
---

Todos os dados criados ou lidos devem ser persistidos em um banco de dados mongodb. Fique livre para decidir um nome de banco e de collection.

Crie dentro da pasta api um arquivo que irá conter o MethodView ou os routes descritos a seguir:

**GET /user/**

Retorna em JSON uma lista com todos os dados de todos os usuários.
O retorno é sempre 200.

**GET /user/\<id\>/**

Retorna em JSON o objeto do usuário filtrado pela chave primária id, caso exista.

Se o usuário não existir, retornar o código HTTP 404, com uma mensagem em JSON com: {"error": "user not found"}

**POST /user/**

Cria um usuário com os dados passados. A requisição será realizada em JSON, com os seguintes dados no corpo:

```json
{
    "first_name" : "",
    "last_name" : "",
    "login" : "",
    "plaintext_password" : "",
}
```

O ID do usuário deverá ser gerado pelo servidor, e não passado pela requisição.

O campo plaintext_password deverá ser convertido para o atributo password do modelo User utilizando o método hash_password

O retorno deverá ser com código HTTP 200, com um JSON indicando o ID do usuário que foi criado. Ex: {"_id": "123456abc"}.


**PUT /user/\<id\>/**

Atualiza os dados de um usuário já criado, filtrado pelo id passado na URL.

Se o usuário não existir, retornar o código HTTP 404, com uma mensagem em JSON com: {"error": "user not found"}.

A requisição será realizada em JSON, com os seguintes dados no corpo:

```json
{
    "first_name" : "",
    "last_name" : "",
    "login" : "",
    "plaintext_password" : "",
}
```


**DELETE /user/\<id\>/**

Deleta um usuário criado, filtrado pelo id passado na URL.

Se o usuário não existir, retornar o código HTTP 404, com uma mensagem em JSON com: {"error": "user not found"}.


**POST /login/**

A requisição será realizada em JSON, com os seguintes dados no corpo:

```json
{
    "login" : "",
    "password" : "",
}
```

O campo password receberá uma senha em plaintext, que será validada a partir do usuário que possui o login correspondente pelo método validate_password.

Caso não exista nenhum usuário com o login fornecido, retorne um 404 com o json: {"error": "user not found"}.

Caso o usuário exista, mas a senha não esteja correta, retorne um HTTP 401 com o json: {"error": "invalid password"}

Caso o usuário exista e senha seja válida, retorne um JSON com um token aleatório (hash). Ex: {"token": "12345689abceef"}



Regras para avaliação
---------------------

**Não utilize bibliotecas de API, como flask-restx ou flask-restful. Programe utilizando o Flask puro, com o decorador @app.route ou a classe MethodView.**

**Não utilize uma biblioteca de ORM (MongoEngine, SQLAlchemy, etc) para a classe de modelo de dados.**


Pontos de avaliação [OBRIGATÓRIO]
---------------------------------

- [ ] Implementar todos os endpoints pedidos
- [ ] Implementar a classe de modelo de dados
- [ ] Persistir os dados no MongoDB
- [ ] Implementar a validação dos dados recebidos da API no POST e PUT (todos os campos são obrigatórios)
- [ ] Realizar commits bem descritos e bom gerenciamento do repositório GIT
- [ ] Documentação


Pontos de avaliação [Opcional/Extra/Diferencial]
------------------------------

- [ ] Criar um sistema de autenticação que permite a utilização dos métodos apenas depois de pegar um token através do login
- [ ] Fazer uma paginação no método GET /user/, que retorna de 10 em 10 resultados e a página é controlada por um parâmetro de query da URL. Ex: /user/?page=2
- [ ] Boa documentação
- [ ] Testes de unidade
